<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PassportAuthController;
use App\Http\Controllers\PosteController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\AttributionController; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

  
Route::post('register', [PassportAuthController::class, 'register']);
Route::post('login', [PassportAuthController::class, 'login']);
     
Route::middleware('auth:api')->group( function () {
    Route::post('/clients/search', [ClientController::class, 'searchClients']);
    Route::resource('clients', ClientController::class);
    Route::post('/attributions/del', [AttributionController::class, 'delete']);
    Route::resource('attributions', AttributionController::class);
    Route::get('/postes/nb-poste', [PosteController::class, 'getNbPoste']);
    Route::get('/postes/get/{id?}/{date?}', [PosteController::class, 'getOrdis']);
    Route::get('/postes/{date?}/{offset?}', [PosteController::class, 'listOrdis']);
    Route::resource('postes', PosteController::class);
});