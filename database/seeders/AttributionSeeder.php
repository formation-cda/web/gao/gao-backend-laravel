<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Attribution;

class AttributionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $data = new Attribution();
        $data->id  = 1;
        $data->date = "10-11-2021";
        $data->horaire = "9";
        $data->clientId = 1;
        $data->posteId = 2;
        $data->save();

        $data2 = new Attribution();
        $data2->id  = 2;
        $data2->date = "9-11-2021";
        $data2->horaire = "11";
        $data2->clientId = 2;
        $data2->posteId = 1;
        $data2->save();

        $data3 = new Attribution();
        $data3->id  = 3;
        $data3->date = "10-11-2021";
        $data3->horaire = "10";
        $data3->clientId = 2;
        $data3->posteId = 3;
        $data3->save();

        $data4 = new Attribution();
        $data4->id  = 4;
        $data4->date = "10-11-2021";
        $data4->horaire = "14";
        $data4->clientId = 3;
        $data4->posteId = 3;
        $data4->save();

        $data5 = new Attribution();
        $data5->id  = 5;
        $data5->date = "10-11-2021";
        $data5->horaire = "14";
        $data5->clientId = 1;
        $data5->posteId = 2;
        $data5->save();
    }
}
