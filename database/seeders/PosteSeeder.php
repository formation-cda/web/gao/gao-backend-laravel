<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Poste;

class PosteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $data = new Poste();
        $data->id  = 1;
        $data->name = "PC1";
        $data->save();

        $data2 = new Poste();
        $data2->id  = 2;
        $data2->name = "PC2";
        $data2->save();

        $data3 = new Poste();
        $data3->id  = 3;
        $data3->name = "PC3";
        $data3->save();

        $data4 = new Poste();
        $data4->id  = 4;
        $data4->name = "PC4";
        $data4->save();
    }
}
