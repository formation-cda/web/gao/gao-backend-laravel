<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Client;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $data = new Client();
        $data->id  = 1;
        $data->firstName = "Dexter";
        $data->lastName = "MORGAN";
        $data->save();

        $data2 = new Client();
        $data2->id  = 2;
        $data2->firstName = "Sherlock";
        $data2->lastName = "HOLMES";
        $data2->save();

        $data3 = new Client();
        $data3->id  = 3;
        $data3->firstName = "Walter";
        $data3->lastName = "BISHOP";
        $data3->save();

        $data4 = new Client();
        $data4->id  = 4;
        $data4->firstName = "Peter";
        $data4->lastName = "BISHOP";
        $data4->save();
    }
}
