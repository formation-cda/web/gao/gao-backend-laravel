<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Models\Poste;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Http\Resources\PosteResource as PosteResource;

class PosteController extends BaseController
{
    
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postes = Poste::all();
    
        return $this->sendResponse([PosteResource::collection($postes)], 'Poste récupéré  avec succès.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $poste = Poste::create($input);
   
        return $this->sendResponse(new PosteResource($poste), 'Poste créé  avec succès.');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $poste = Poste::find($id);
  
        if (is_null($poste)) {
            return $this->sendError('Poste not found.');
        }
   
        return $this->sendResponse([new PosteResource($poste)], 'Poste récupéré  avec succès.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Poste $poste)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $poste->name = $input['name'];
        $poste->save();
   
        return $this->sendResponse(new PosteResource($poste), 'Poste mise à jour  avec succès.');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Poste $poste)
    {
        $poste->delete();
   
        return $this->sendResponse([], 'Poste supprimé  avec succès.');
    }

    /**
     * Get list attribution par ordi
     *
     * @param  Request  $request
     * @return Response
     */
    public function listOrdis(Request $request)
    {
        $input = $request->all();
        $date = $input['date'];
        $offset = $input['offset'];
        $limit = 3;
        // Log::debug('Date : '.json_encode($date).' Offset : '.$offset.' Limit : '.$limit);
        $listOrdi =  Poste::with(array('attribution' => function($query) use ($date){
            $query -> where('date',$date);
        }))
        ->skip($offset)
        ->limit($limit)
        ->orderBy('name', 'ASC')
        ->get();
        
        // Log::debug(json_encode($listOrdi).'Attrib');
        return $this->sendResponse(PosteResource::collection($listOrdi), 'Poste récupéré  avec succès.');
    }
    
    /**
     * Get list attribution par ordi
     *
     * @param  Request  $request
     * @return Response
     */
    public function getOrdis(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];
        $date = $input['date'];
        $poste =  Poste::where('id', $id)
        ->with(array('attribution' => function($query) use ($date){
            $query -> where('date',$date);
        }))
        ->get()
        ->first();
        
        Log::debug(json_encode($poste).'poste');
        return $this->sendResponse(new PosteResource($poste), 'Poste récupéré  avec succès.');
    }

      /**
     * Get a number of poste.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNbPoste()
    {
        $nbPoste = Poste::all()->count();
    
        return $this->sendResponse($nbPoste, 'Poste récupéré  avec succès.');
    }
}
