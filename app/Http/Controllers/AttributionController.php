<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Models\Attribution;
// use Illuminate\Support\Facades\Log;
use Validator;
use App\Http\Resources\AttributionResource as AttributionResource;

class AttributionController extends BaseController
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attribution = Attribution::all();
    
        return $this->sendResponse(AttributionResource::collection($attribution), 'Attribution récupéré  avec succès.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        // Log::debug(json_encode($input).' An informational message.');
   
        $validator = Validator::make($input, [
            'horaire' => 'required',
            'date' => 'required',
            'clientId' => 'required',
            'posteId' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $attribution = Attribution::create($input);
   
        return $this->sendResponse(new AttributionResource($attribution), 'Attribution effectué avec succès.');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attribution = Attribution::find($id);
  
        if (is_null($attribution)) {
            return $this->sendError('Attribution introuvable.');
        }
   
        return $this->sendResponse(new AttributionResource($attribution), 'Attribution récupéré avec succès  avec succès.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attribution $attribution)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'horaire' => 'required',
            'date' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $attribution->name = $input['name'];
        $attribution->save();
   
        return $this->sendResponse(new AttributionResource($attribution), 'Attribution misje à jour avec succès.');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribution $attribution)
    {
        $attribution->delete();
   
        return $this->sendResponse([], 'Attribution supprimé  avec succès.');
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $input = $request->all();

        // Log::debug(json_encode($input).'input');
        $attribution = Attribution::find($input['id']);

        // Log::debug(json_encode($attribution).'attribution');
        $res = $attribution -> delete();
  
        if(!$res) {
            return $this->sendError('Attribution introuvable.');
        }
   
        return $this->sendResponse([], 'Attribution supprimé avec succès.');
    }
}
