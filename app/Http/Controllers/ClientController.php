<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Models\Client;
use Validator;
use App\Http\Resources\ClientResource as ClientResource;

class ClientController extends BaseController
{
    /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function index()
      {
          $clients = Client::all();
      
          return $this->sendResponse([ClientResource::collection($clients)], 'Client récupéré avec succès.');
      }
    /**
     * Store a newly créé resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'lastName' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $client = Client::create($input);
   
        return $this->sendResponse(new ClientResource($client), 'Client créé avec succès.');
    } 

   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);
  
        if (is_null($Client)) {
            return $this->sendError('Client not found.');
        }
   
        return $this->sendResponse([new ClientResource($Client)], 'Client récupéré avec succès.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $client->name = $input['name'];
        $client->save();
   
        return $this->sendResponse(new ClientResource($Client), 'Client ùise à jour avec succès.');
    }
   

     /**
     * Get list clients par ordi
     *
     * @param  Request  $request
     * @return Response
     */
    public function searchClients(Request $request)
    {
        $word = $request->word;
        $listClients =  Client::where('lastName','LIKE',"%".$word."%")
        ->orWhere('firstName','LIKE',"%".$word."%")
        ->get();
        return $this->sendResponse(ClientResource::collection($listClients), 'Client récupéré avec succès.');
    }
}
