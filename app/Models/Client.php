<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Attribution;

class Client extends Model
{
    use HasFactory;
    
    protected $table = 'clients';
    protected $fillable = ['lastName', 'firstName'];
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
        protected $primaryKey = 'id';
        /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Get the attribution for the client.
    */
    public function attribution()
    {
        return $this->hasMany(Attribution::class, 'clientId');
    }    
}
