<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Client;
use App\Models\Attribution;

class Poste extends Model
{
    use HasFactory;
    
    protected $table = 'postes';
    protected $fillable = ['name'];
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
        protected $primaryKey = 'id';
        /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    
    /*
    *
    * Get the attribution for the ordi.
    *
    */
    public function attribution()
    {
        return $this->hasMany(Attribution::class, 'posteId');
    }
}
