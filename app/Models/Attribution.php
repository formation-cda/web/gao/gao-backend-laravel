<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Client;
use App\Models\Potse;

class Attribution extends Model
{
    use HasFactory;
    protected $table = 'attributions';
    protected $fillable = ['date', 'horaire', 'clientId', 'posteId'];
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
        protected $primaryKey = 'id';
        /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Get the client that owns the attribution.
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'clientId');
    }

    /**
     * Get the ordi that owns the attribution.
     */
    public function postes()
    {
        return $this->belongsTo(Potse::class, 'posteId');
    }
}
